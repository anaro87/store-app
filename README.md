
# project setup
- this project was created using MacOS Monterey, laravel v8.6.12
- composer is needed to run this project
- clone the project
- go to project's folder and run "composer install"
- rename .env.example for .env and add this content:
```
APP_NAME=Laravel
APP_ENV=local
APP_KEY=base64:ajB5YbbosKj/QmhLCdrWdkdx0pyDuhl0J2rWWw7HRgs=
APP_DEBUG=true
APP_URL=http://localhost

LOG_CHANNEL=stack
LOG_DEPRECATIONS_CHANNEL=null
LOG_LEVEL=debug

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=store_app
DB_USERNAME=root
DB_PASSWORD=

BROADCAST_DRIVER=log
CACHE_DRIVER=file
FILESYSTEM_DRIVER=local
QUEUE_CONNECTION=sync
SESSION_DRIVER=file
SESSION_LIFETIME=120

MEMCACHED_HOST=127.0.0.1

REDIS_HOST=127.0.0.1
REDIS_PASSWORD=null
REDIS_PORT=6379

MAIL_MAILER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
MAIL_FROM_NAME="${APP_NAME}"

AWS_ACCESS_KEY_ID=
AWS_SECRET_ACCESS_KEY=
AWS_DEFAULT_REGION=us-east-1
AWS_BUCKET=
AWS_USE_PATH_STYLE_ENDPOINT=false

PUSHER_APP_ID=
PUSHER_APP_KEY=
PUSHER_APP_SECRET=
PUSHER_APP_CLUSTER=mt1

MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"
MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"
```

- to run the project: php artisan serve --port=9001
- in browser: http://127.0.0.1:9001
- no database is need it
- run test: php artisan test

# install composer in MacOS
In Terminal
- curl -sS https://getcomposer.org/installer | php
- sudo mv composer.phar /usr/local/bin/composer
- verify installation:  composer

# install laravel in MacOS
- composer global require "laravel/installer"

# Algorithm Solution Summary
My approach was to try to create the major quantity of subsets with the max discount possible, based on my needs I read about the set packing problem
and use it as a starting point and apply some mathematical operations in order to help me to calculate these subsets with major speed.

After analyzing the problem I found some important points: 
1. 5 its the the max number of types to get discounts
2. 2 its the less number of types to get discount
3. our discounts are paired values[2=>5%, 3=> 10%, 4=>20%, 5=>25%] 

My algorithm was divided in various sections
1. I need to get the total number of type selected by the user
2. I need to get the total number of items
3. calculate the groups(subset) - the core function
4. calculate the discount per group 
    - calculate single discount per group
    - calculate group total discount


Why are 1 and 2 so important? because based on those 2 values I will calculate the groups(subsets) with the best discount possible.
How do I do this? 
### 1.  well first I found the basics cases: when user select 1 tshirt per type, if we search for combinations and solve the problem manually the best discount is generated with 1 group with the total of items for example:
- 5 items & 5 types - 25%
- 4 items & 4 types - 20% and so on

### 2. but if the total of items its > that the total of types we have: First We need to find if the total of items is multiple of the total of types, this in order to get the number of items per group, for example:

1.  For the given example A=2, B=2, C=2, D=1, E=1. We have a total of 8 items and 5 groups
8 is != that 5
8 is > than 5 this means we can try to get a group of 5 items to get a subset with the major discount of 25% but even though we can divide 8/5, 8 is not multiple of 5 so our best approach with a subset of 5 items is discarded. So now we need to move with the second best discount subset which its 4 items per subsets with a discount of 20% as the next mayor discount possible and 8 is multiple of 4, 8/4 = 2 with 0 remainder this means we are going to create 2 groups of 4 items with 20% discount each

2.  But what happens if the total is not multiple of any of the paired discount numbers? we are going to manage then as prime number for this cases we are going to create 
the groups based on 3 important numbers: 
-  divisor which is the total of items per group associated to the discount %
- quotient which is the number of groups of divisor items
-  remainder the group with the rest of items
-  For this case we are going to start the division with the total of types selected by the user, 
- for example 1: for this set: A=3, B=3, C=3, D=2, E=2
We have a total of 13 items and 5 types. 13/5 = 2 with 3 remainder this means we are going to have:  2 groups of 5 with 25% of discount each && 1 group of 3 items with 10% as the best option. 
- for example 2: for this set: A=20, B=2, C=1
We have a total of 23 items and 3 types. 23/3 = 7 with a remainder of 2 this means we are going to have: 7 groups of 3 items with 10% discount of discount each && 1 group of 2 items with 5% as the best option


