<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Shirt;
use App\Models\Discount;

class Cart extends Controller
{
    
    /**
     * index
     *
     * @return view
     */
    public function index()
    {
        $shirt_a = new Shirt("A",1, 8, 'images/japanShirt.jpeg');
        $shirt_b = new Shirt("B",1, 8,'images/naruto.jpeg');
        $shirt_c = new Shirt("C",1, 8, 'images/vegeta.jpeg');
        $shirt_d = new Shirt("D",1, 8, 'images/kitty.jpeg');
        $shirt_e = new Shirt("E",1, 8, 'images/kuromi.jpeg');
        $data = array($shirt_a,$shirt_b, $shirt_c, $shirt_d, $shirt_e);
        
        return view('cart', [
            'data' => $data
        ]);
    }

    /**
     * calculateDiscount
     *
     * @param Request $request
     * @return array
     */
    public function calculateDiscount(Request $request){
        $order = $request->all();
        $discount = new Discount(8, array(2=>0.05, 3=>0.1, 4=>0.2, 5=>0.25));
        $result = $discount->discount($order["cartOrder"]);

        return array("result" => $result);
    }

    
}
