<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Discount extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    private $discountPercent;
    private $result;
    private $price;

    /**
     * __construct
     *
     * @param [float] $price
     * @param [array] $percents [qty=>discountPercent, qty=> discountPercent]
     */
    public function __construct($price, $percents) {
        $this->discountPercent = $percents;
        $this->price = $price;
    }

    /**
     * discount function
     * calculates the best discount per groups
     * @author ana.rodriguez <anaro87@gmail.com>
     * @param [array] $shirtsArr
     * @return array
     */
    public function discount($shirtsArr){
        $result = [];
        $numberOfTypes = $this->getNumberOfTypes($shirtsArr);
        $totalItems = $this->getTotalItems($shirtsArr);
        $groups = $this->createGroups($numberOfTypes, $totalItems);
        //if groups != [] calculateDiscount else calculate total with no discount
        $groupsDiscountResults = $this->calculateDiscountPerGroup($groups);

        return $groupsDiscountResults;


    }

    /**
     * getNumberOfTypes function
     * get te number of shirt types selected by the user answer the question:
     * How many types of shirts that the user are going to buy? 
     * @author ana.rodriguez <anaro87@gmail.com>
     * @param [array] $shirtsArr
     * @return int
     */
    public function getNumberOfTypes($shirtsArr){
        return count($shirtsArr);
    }

    /**
     * getTotalItems function
     * gets the total of items selected by the user, the sum of all qtys, answer the questions
     * What is the total of items that the user are going to buy?
     * @author ana.rodriguez <anaro87@gmail.com>
     * @param [array] $shirtsArr
     * @return int
     */
    public function getTotalItems($shirtsArr){
        $totalItems = 0;
        for($i = 0; $i < count($shirtsArr); $i++){
            if(isset($shirtsArr[$i]))
            {
                $totalItems = $totalItems +  intval($shirtsArr[$i]["qty"]);
            }
        }
        return $totalItems;
    }

    /**
     * createGroups function
     * core function use to create the best groups to get the max discount possible
     * @author ana.rodriguez <anaro87@gmail.com>
     * @param [int] $numberOfTypes
     * @param [int] $totalItems
     * @return array
    */
    public function createGroups($numberOfTypes, $totalItems){
        $result = [];
        $discounts = $this->discountPercent;
        
        if($numberOfTypes == 1){
            return [];
        }

        if($totalItems == $numberOfTypes && $numberOfTypes > 1){
            array_push($result,array("numberOfGroups"=>1, "numberOfShirtsPerGroups"=> $totalItems, "percent"=> $discounts[$numberOfTypes]));
            return $result;
        }else{
            if(fmod($totalItems, $numberOfTypes) == 0){
                $quotient = (int) $this->getNumberOfGroups($totalItems, $numberOfTypes); 
                array_push($result,array("numberOfGroups"=>$quotient, "numberOfShirtsPerGroups"=> $numberOfTypes, "percent"=> $discounts[$numberOfTypes]));
                return $result;
            }else {
                //let's find of which number of type is multiple the $totalItems, 
                //has to be multiple of one of these [2,3,4,5] which are the qtys with discounts
                $modOf = $this->getModByType($totalItems, $numberOfTypes);
                // var_dump("$totalItems is multiple of $modOf");
                if($modOf != -1){
                    $quotient = (int) $this->getNumberOfGroups($totalItems, $modOf);
                    array_push($result,array("numberOfGroups"=>$quotient, "numberOfShirtsPerGroups"=> $modOf, "percent"=> $discounts[$modOf])); 
                }else{
                    // number can be a prime number and no multiple of any of [2,3,4,5]
                    $groups = $this->primeGroups($totalItems, $numberOfTypes);
                    $result = $groups;
                    // var_dump("**** PRIME GROUPS ***");
                    // var_dump($result);
                }
                
            }
        }
        

        return $result;
    }

    /**
     * calculateDiscountPerGroup function
     * creates the array that will we used to display information in the front end
     * we use the single discount per group to calculate the group discount 
     * for example if we have {7 groups of 3 shirt,  1 group of 2 shirts}
     * singleDiscount=2.4 for first group
     * GroupTotalAmount=151.2
     * @author ana.rodriguez <anaro87@gmail.com>
     * @param [array] $groups
     * [array("numberOfGroups"=>7,"numberOfShirtsPerGroups"=>3,"percent"=>0.1,"groupTotalAmount"=>151.20),
     *  array("numberOfGroups"=>1,"numberOfShirtsPerGroups"=>2,"percent"=>0.05,"groupTotalAmount"=>15.2)
     * ]
     * @return array
    */
    public function calculateDiscountPerGroup($groups){
        $groupTotalAmount = 0;
        $groupResultArr = [];

        for ($i=0; $i < count($groups); $i++) {
            $group = $groups[$i];
            $singleDiscount = $this->calculateSingleDiscount($this->price, $group["numberOfShirtsPerGroups"], $group["percent"]);
            $groupTotalAmount = $this->calculateGroupTotalAmount($this->price, $group["numberOfShirtsPerGroups"], $group["numberOfGroups"], $singleDiscount);
            $group['groupTotalAmount'] = $groupTotalAmount;
            array_push($groupResultArr, $group);
        }

        return $groupResultArr;

    }

    /**
     * getNumberOfGroups function
     * calculates the number of groups need, we use the quotient
     * @param [int] $numberItems
     * @param [int] $numberType
     * @return int
     */
    public function getNumberOfGroups($totalItems, $numberOfTypes){
        $quotient = (int) ($totalItems/$numberOfTypes); 
        return $quotient;
    }

    /**
     * getModByType function
     * return the number that is going to be use as numberOfShirtsPerGroups 
     * if is multiple of one of these [2,3,4,5] which are the qtys with discounts
     * its use to generate a more direct way of calculation
     * @param [int] $totalItems
     * @param [int] $numberOfType
     * @return int
     */
    public function getModByType($totalItems, $numberOfType){
		$modOf = $numberOfType;
        $isMultiple = -1;
        while ($isMultiple != 0 && $modOf >= 2){
        	$isMultiple = fmod($totalItems, $modOf);
            if($isMultiple != 0){
            	$modOf--;
            }
        }
        
        if($isMultiple == 0){
        	return $modOf;
        }else{
        	return -1;
        }

    }

    /**
     * calculateSingleDiscount function
     * calculates the single discount per group, for example if we have {7 groups of 3 shirt and 1 group of 2 shirts}
     * calculations: (8X3) * 0.1 = 2.4 => amount to discount
     * @author ama.rodriguez <anaro87@gmail.com>
     * @param [float] $price
     * @param [int] $numberOfShirtsPerGroups
     * @param [float] $percent
     * @return float
     */
    public function calculateSingleDiscount($price, $numberOfShirtsPerGroups, $percent){
        $singleDiscount = ($price * $numberOfShirtsPerGroups) * $percent;

        return $singleDiscount;
    }

    /**
     * calculateGroupTotalAmount function
     * use to calculate the total amount of discount per group, for example if we have {7 groups of 3 shirt and 1 group of 2 shirts}
     * calculations: 
     * (8*3) - 2.4 = 21.6
     * 21.6 * 7 = 151.2 total amount to charge after discount for the first group group
     * @author ana.rodriguez <anaro87@gmail.com> 
     * @param [float] $price
     * @param [int] $numberOfShirtsPerGroups
     * @param [int] $numberOfGroups
     * @param [float] $singleDiscount
     * @return float
     */
    public function calculateGroupTotalAmount($price, $numberOfShirtsPerGroups, $numberOfGroups, $singleDiscount){
        // var_dump("calculateGroupTotalAmount");
        // var_dump(" price: $price, numberOfShirtsPerGroups: $numberOfShirtsPerGroups, numberOfGroups : $numberOfGroups, singleDiscount:   $singleDiscount");
        $singleAmount = ($price * $numberOfShirtsPerGroups) - $singleDiscount;
        $totalAmount = $singleAmount * $numberOfGroups;

        return $totalAmount;
    }

    /**
     * primeGroups
     * @author ana.rodriguez <anaro87@gmail.com> 
     * @param [int] $totalItems
     * @param [int] $numberOfTypes
     * @return array
     */
    public function primeGroups($totalItems, $numberOfTypes){
        $groups = [];
        $discounts = $this->discountPercent;

        $quotient = (int) ($totalItems/$numberOfTypes);
        array_push($groups,array("numberOfGroups"=>$quotient, "numberOfShirtsPerGroups"=> $numberOfTypes, "percent"=> $discounts[$numberOfTypes]));
        $remainder = $totalItems % $numberOfTypes; 
        if($remainder > 1){
            array_push($groups,array("numberOfGroups"=>1, "numberOfShirtsPerGroups"=> $remainder, "percent"=> $discounts[$remainder]));
        }else {
            array_push($groups,array("numberOfGroups"=>1, "numberOfShirtsPerGroups"=> 1, "percent"=> 0));
        }
        return $groups;
        
    }
}
