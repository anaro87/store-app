
<html>
    <head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>   

    </head>
    <body>
    <section class="h-100" style="background-color: #FFF;">
        <div class="container h-100 py-4">
        <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-10">

        <div class="d-flex justify-content-between align-items-center mb-4">
          <h3 class="fw-normal mb-0 text-black">Shopping Cart</h3>
          <div>
            <p class="mb-0"><span class="text-muted">Sort by:</span> <a href="#!" class="text-body">price <i
                  class="fas fa-angle-down mt-1"></i></a></p>
          </div>
        </div>
        <?php

            foreach ($data as $shirt) { 
                $img = $shirt->img;
                        
                $html = '
                        <div id="div_'.$shirt->type.'" class="card rounded-3 mb-4">
                        <div class="card-body p-4">
                        <div class="row d-flex justify-content-between align-items-center">
                            <div class="col-md-2 col-lg-2 col-xl-2">
                                <img
                                src="'. asset($shirt->img).'"
                                class="img-fluid rounded-3" alt="Cotton T-shirt">
                            </div>
                            <div class="col-md-3 col-lg-3 col-xl-3">
                                <p class="lead fw-normal mb-2">Basic T-shirt</p>
                                <p>Type:<span id="type_sp_'.$shirt->type.'" class="text-muted">'. $shirt->type .' </span></p>
                            </div>
                            <div class="col-md-3 col-lg-3 col-xl-2 d-flex">
                                <button class="btn btn-link px-2"
                                onclick="this.parentNode.querySelector(\'input[type=number]\').stepDown()">
                                <i class="fas fa-minus"></i>
                                </button>
        
                                <input id="qty_'.$shirt->type.'" min="0" name="quantity" value="1" type="number"
                                class="form-control form-control-sm" />
        
                                <button class="btn btn-link px-2"
                                onclick="this.parentNode.querySelector(\'input[type=number]\').stepUp()">
                                <i class="fas fa-plus"></i>
                                </button>
                            </div>
                            <div class="col-md-3 col-lg-2 col-xl-2 offset-lg-1">
                                <h5 class="mb-0">$<span id="price_sp_'.$shirt->type.'">'. $shirt->price .'.00</span></h5>
                            </div>
                            <div class="col-md-1 col-lg-1 col-xl-1 text-end">
                                <button type="button" class="btn btn-danger btn-block btn-lg" onclick="deleteItem(\''.$shirt->type.'\')"><i class="fa-solid fa-trash-can"></i></button>
                                
                            </div>
                        </div>
                        </div>
                        </div>';

                echo $html;
            }
                
        ?>

        <div class="card rounded-3 mb-4">
            <div class="card-body">
            <?php 
                $typesArr = array_map(function ($object) { return $object->type; }, $data);
                $types = implode(',', $typesArr);
                echo '<button type="button" class="btn btn-warning btn-block btn-lg" onclick="calculateDiscount(\''.$types.'\')">Calculate Discount</button>'; 
            ?>
            </div>
        </div>

        <div id="best-discounts" class="card rounded-3 mb-4" style="display: none;">
            <div class="card-body">
            <h3>Best Discounts Summary</h3>
                <table class="table">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col"></th>
                    </tr>
                </thead>
                <tbody id="best-discount-body">
                    
                </tbody>
                </table>
            </div>
        </div>

        </div>
        </div>
        </div>
    </section>
    </body>
</html>

<script>
     /**
     * calculateDiscount
     * @author ana.rodriguez <anaro87@gmail.com>
     * @param [string] $shirtsType
     */
    function calculateDiscount(shirtsType){
        let shirt;
        const shirtsArr = [];
        const typesArr = shirtsType.split(",")

        for(i=0; i< typesArr.length; i++){
            let type = typesArr[i];

            shirt = {
                    "type": type,
                    "qty": $(`#qty_${type}`).val(),
                    "price": $(`#price_sp_${type}`).html(),
                }
            shirtsArr.push(shirt);
        }
        const cartOrder = shirtsArr.filter(shirt => shirt.qty != undefined);

        let data = {
            cartOrder: cartOrder
        }
    
        $.ajax({
            method: "POST",
            dataType: "json",
            url: "{{ URL::to('/') }}/calculate-discount",
            data: data,
            headers: {'X-CSRF-TOKEN':"{{ csrf_token() }}"},
            }).done(function( data ) {
                bodyContent = "";
                discountSummary =  data.result;
                i = 1;
                total = 0;

                for (var obj of discountSummary) {
                    percent = obj.percent * 100;
                    total = total + obj.groupTotalAmount;
                    groupLabel = obj.numberOfGroups == 1? "group" : "groups";
                    groupTotalAmount = parseFloat(obj.groupTotalAmount).toFixed(2);
                    bodyContent += `<tr>
                                    <th scope="row">${i++}</th>
                                    <td><strong>${obj.numberOfGroups}</strong> ${groupLabel} of <strong>${obj.numberOfShirtsPerGroups}</strong> shirts 
                                    with <strong>${percent}%</strong> discount at <strong>$${groupTotalAmount}</strong></td>
                                    </tr>`;
                }

                $('#best-discounts').show();
                total = parseFloat(total).toFixed(2);
                totalRow = `<tr style= 'color: #4CAF50;'>
                            <th scope="row"><h5>TOTAL AMOUNT</h5></th>
                            <td><h5>$${total}</h5></td>
                            </tr>
                            `;
                tableBody = bodyContent + totalRow;

                $('#best-discount-body').html(tableBody);
               
            })
            .fail(function(error) {
                alert( "error" );
                console.log(error);
            });
   }

   function deleteItem(type){
        $(`#div_${type}`).remove();
    }

</script>
