<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Discount;

class CartTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_cart_index()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_cart()
    {
        $response = $this->get('/cart');

        $response->assertStatus(200);
    }

    public function test_cart_to_discount()
    {
        // Session::start();
        $discount = new Discount(8, array(2=>0.05, 3=>0.1, 4=>0.2, 5=>0.25));
        $expectedResult = []; 
        array_push($expectedResult, array("numberOfGroups"=>1,"numberOfShirtsPerGroups"=>5, "percent"=>0.25, "groupTotalAmount"=>30));    
        
        $shirtArr = [];
        array_push($shirtArr, array("type"=>"A","qty"=>1, "price"=>"8.00"));
        array_push($shirtArr, array("type"=>"B","qty"=>1, "price"=>"8.00"));
        array_push($shirtArr, array("type"=>"C","qty"=>1, "price"=>"8.00"));
        array_push($shirtArr, array("type"=>"D","qty"=>1, "price"=>"8.00"));
        array_push($shirtArr, array("type"=>"E","qty"=>1, "price"=>"8.00"));

        $result=$discount->discount($shirtArr);
        
        $response = $this->withHeaders([
            'X-CSRF-TOKEN' => csrf_token(),
        ])->post('/calculate-discount', ['cartOrder' => $shirtArr]);

        $response->assertStatus(200);
    }
}
