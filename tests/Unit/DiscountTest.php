<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Models\Discount;

class DiscountTest extends TestCase
{

    /**
     * test_discount function
     * use to test discount function
     * @author ana.rodriguez <anaro87@gmail.com>
     * @return bool
     */
    public function test_discount()
    {
        
        $discount = new Discount(8, array(2=>0.05, 3=>0.1, 4=>0.2, 5=>0.25));
        $expectedResult = []; 
        array_push($expectedResult, array("numberOfGroups"=>1,"numberOfShirtsPerGroups"=>5, "percent"=>0.25, "groupTotalAmount"=>30));    
        
        $shirtArr = [];
        array_push($shirtArr, array("type"=>"A","qty"=>1, "price"=>"8.00"));
        array_push($shirtArr, array("type"=>"B","qty"=>1, "price"=>"8.00"));
        array_push($shirtArr, array("type"=>"C","qty"=>1, "price"=>"8.00"));
        array_push($shirtArr, array("type"=>"D","qty"=>1, "price"=>"8.00"));
        array_push($shirtArr, array("type"=>"E","qty"=>1, "price"=>"8.00"));

        $result=$discount->discount($shirtArr);
        
        
        $this->assertEquals($expectedResult, $result);

    }

    /**
     * test_getNumberOfTypes function
     * @author ana.rodriguez <anaro87@gmail.com>
     * @return bool
     */
    public function test_getNumberOfTypes(){
        $discount = new Discount(8, array(2=>0.05, 3=>0.1, 4=>0.2, 5=>0.25));

        $shirtArr = [];
        array_push($shirtArr, array("type"=>"A","qty"=>1, "price"=>"8.00"));
        array_push($shirtArr, array("type"=>"B","qty"=>1, "price"=>"8.00"));
        array_push($shirtArr, array("type"=>"C","qty"=>1, "price"=>"8.00"));
        array_push($shirtArr, array("type"=>"D","qty"=>1, "price"=>"8.00"));
        array_push($shirtArr, array("type"=>"E","qty"=>1, "price"=>"8.00"));

        $result = $discount->getNumberOfTypes($shirtArr);
        $this->assertEquals(5, $result);

    }

    /**
     * test_getTotalItems function
     * @author ana.rodriguez <anaro87@gmail.com>
     * @return bool
     */
    public function test_getTotalItems(){
        $discount = new Discount(8, array(2=>0.05, 3=>0.1, 4=>0.2, 5=>0.25));

        $shirtArr = [];
        array_push($shirtArr, array("type"=>"A","qty"=>2, "price"=>"8.00"));
        array_push($shirtArr, array("type"=>"B","qty"=>2, "price"=>"8.00"));
        array_push($shirtArr, array("type"=>"C","qty"=>2, "price"=>"8.00"));
        array_push($shirtArr, array("type"=>"D","qty"=>2, "price"=>"8.00"));
        array_push($shirtArr, array("type"=>"E","qty"=>2, "price"=>"8.00"));

        $result = $discount->getTotalItems($shirtArr);
        $this->assertEquals(10, $result);

    }

    /**
     * createGroups function
     * use to test createGroups for the most basic case
     * @author ana.rodriguez <anaro87@gmail.com>
     * @return bool
     */
    public function test_createGroups_basic(){
        $discount = new Discount(8, array(2=>0.05, 3=>0.1, 4=>0.2, 5=>0.25));
        $numberOfTypes = 5;
        $totalItems = 5;
        $expectedResult = []; 
        array_push($expectedResult, array("numberOfGroups"=>1,"numberOfShirtsPerGroups"=>5, "percent"=>0.25));    

        $result = $discount->createGroups($numberOfTypes, $totalItems);

        $this->assertEquals($expectedResult, $result);

    }

    /**
     * test_createGroups_medium function
     * use to test createGroups for medium complex case
     * @author ana.rodriguez <anaro87@gmail.com>
     * @return bool
     */
    public function test_createGroups_medium(){
        $discount = new Discount(8, array(2=>0.05, 3=>0.1, 4=>0.2, 5=>0.25));
        $numberOfTypes = 5;
        $totalItems = 8;
        $expectedResult = []; 
        array_push($expectedResult, array("numberOfGroups"=>2,"numberOfShirtsPerGroups"=>4, "percent"=>0.2));    

        $result = $discount->createGroups($numberOfTypes, $totalItems);

        $this->assertEquals($expectedResult, $result);

    }

    /**
     * test_createGroups_complex function
     * use to test createGroups for complex case
     * @author ana.rodriguez <anaro87@gmail.com>
     * @return bool
     */
    public function test_createGroups_complex(){
        $discount = new Discount(8, array(2=>0.05, 3=>0.1, 4=>0.2, 5=>0.25));
        $numberOfTypes = 5;
        $totalItems = 13;
        $expectedResult = []; 
        array_push($expectedResult, array("numberOfGroups"=>2,"numberOfShirtsPerGroups"=>5, "percent"=>0.25)); 
        array_push($expectedResult, array("numberOfGroups"=>1,"numberOfShirtsPerGroups"=>3, "percent"=>0.1));   

        $result = $discount->createGroups($numberOfTypes, $totalItems);

        $this->assertEquals($expectedResult, $result);

    }

    /**
     * test_calculateDiscountPerGroup function
     * @author ana.rodriguez <anaro87@gmail.com>
     * @return bool
     */
    public function test_calculateDiscountPerGroup(){
        $discount = new Discount(8, array(2=>0.05, 3=>0.1, 4=>0.2, 5=>0.25));

        $expectedResult = []; 
        array_push($expectedResult, array("numberOfGroups"=>2,"numberOfShirtsPerGroups"=>5, "percent"=>0.25, "groupTotalAmount"=>60)); 
        array_push($expectedResult, array("numberOfGroups"=>1,"numberOfShirtsPerGroups"=>3, "percent"=>0.1, "groupTotalAmount" => 21.6));

        $groups = []; 
        array_push($groups, array("numberOfGroups"=>2,"numberOfShirtsPerGroups"=>5, "percent"=>0.25)); 
        array_push($groups, array("numberOfGroups"=>1,"numberOfShirtsPerGroups"=>3, "percent"=>0.1));

        $result=$discount->calculateDiscountPerGroup($groups);

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * test_getNumberOfGroups function
     * @author ana.rodriguez <anaro87@gmail.com>
     * @return bool
     */
    public function test_getNumberOfGroups(){
        $discount = new Discount(8, array(2=>0.05, 3=>0.1, 4=>0.2, 5=>0.25));
        $totalItems = 8;
        $numberOfTypes = 4;

        $result = $discount->getNumberOfGroups($totalItems,$numberOfTypes);

        $this->assertEquals(2, $result);
    }

    /**
     * test_getModByType function
     * @author ana.rodriguez <anaro87@gmail.com>
     * @return bool
     */
    public function test_getModByType(){
        $discount = new Discount(8, array(2=>0.05, 3=>0.1, 4=>0.2, 5=>0.25));
        $totalItems = 8;
        $numberOfTypes = 4;

        $result = $discount->getModByType($totalItems,$numberOfTypes);

        $this->assertEquals(4, $result);
    }

     /**
     * test_calculateSingleDiscount function
     * @author ana.rodriguez <anaro87@gmail.com>
     * @return bool
     */
    public function test_calculateSingleDiscount(){
        $discount = new Discount(8, array(2=>0.05, 3=>0.1, 4=>0.2, 5=>0.25));

        $result = $discount->calculateSingleDiscount(8, 4, 0.20);

        $this->assertEquals(6.4, $result);

    }

    /**
     * test_calculateGroupTotalAmount function
     * @author ana.rodriguez <anaro87@gmail.com>
     * @return bool
     */
    public function test_calculateGroupTotalAmount(){
        $discount = new Discount(8, array(2=>0.05, 3=>0.1, 4=>0.2, 5=>0.25));
        $price = 8;
        $numberOfShirtsPerGroups= 4;
        $numberOfGroups=2;
        $singleDiscount=6.4;

        $result = $discount->calculateGroupTotalAmount($price, $numberOfShirtsPerGroups, $numberOfGroups, $singleDiscount);

        $this->assertEquals(51.20, $result);
    }

    /**
     * test_primeGroups function
     * @author ana.rodriguez <anaro87@gmail.com>
     * @return bool
     */
    public function test_primeGroups(){
        $discount = new Discount(8, array(2=>0.05, 3=>0.1, 4=>0.2, 5=>0.25));
        $totalItems=13;
        $numberOfTypes=5;

        $expectedResult = []; 
        array_push($expectedResult, array("numberOfGroups"=>2,"numberOfShirtsPerGroups"=>5, "percent"=>0.25)); 
        array_push($expectedResult, array("numberOfGroups"=>1,"numberOfShirtsPerGroups"=>3, "percent"=>0.1));

        $result = $discount->primeGroups($totalItems, $numberOfTypes);

        $this->assertEquals($expectedResult, $result);
    }

    
}
